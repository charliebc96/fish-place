

## Clonar repositorio

1. por cmd ir a la ruta **/dev/php**
2. ejecutar el comando **git clone https://ferneyiridian@bitbucket.org/ferneyiridian/impodisnal.git**
3. seguir los pasos de [creacion de proyecto](https://docs.google.com/document/d/12FKuUKcRmBHQHWqHSwG4OAKWdMITrQjQy_-WfSjMfZ4/edit#heading=h.gh9s1x4tm2mu) **(composer update, npm install, bower, gulp, etc.)**
4. para ejecutar composer usar uno de estos comandos 
	**php -d memory_limit=-1 C:\ProgramData\ComposerSetup\bin\composer.phar update**
	
	**C:\php\php -d memory_limit=-1 C:\ProgramData\ComposerSetup\bin\composer.phar update**

---

## Hacer un commit


1. Ejecutar el comando **git status**
2. Revisar que los archivos a agregar/modificar sean los correctos (por ejemplo que nunca este el .zip del proyecto)
3. Ejecutar el comando **git add .**
4. Ejecutar el comando **git commit -m "mensaje incluyendo el id de la incidencia en Jira"**
5. Ejecutar el comando **git push**
6. En caso de preguntar credenciales poner las personales de bitbucket

---

## Sincronizar local con servidor


1. Ejecutar el comando **git pull**

