var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');

var paths = {
   sass: ['./scss/**/*.scss']
};

gulp.task('default', ['sass','sass-admin']);

gulp.task('sass', function (done) {
    gulp.src('./scss/front/*.scss')
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(gulp.dest('./html/css/'))
        .pipe(minifyCss({keepSpecialComments: 0 }))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('./html/css/'))
        .on('end', done);
});

gulp.task('sass-admin', function (done) {
    gulp.src('./scss/admin/*.scss')
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(gulp.dest('./html/css/admin/'))
        .pipe(minifyCss({keepSpecialComments: 0 }))
        //.pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('./html/css/admin/'))
        .on('end', done);
});

gulp.task('watch', function () {
    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.sass, ['sass-admin']);
});

