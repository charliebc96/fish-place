/* Login Dropdown */

// Switch login-recover
$( "#pass-recover" ).click(function() {
  $( ".login-form" ).toggle();
  $( ".password-form" ).toggle();
});

$( "#login-form" ).click(function() {
  $( ".login-form" ).toggle();
  $( ".password-form" ).toggle();
});

// Dropdown

$( ".login-box-wrapper-toggle" ).mouseenter(function() {
  $( ".login-box" ).toggle();
  $( ".login-screen" ).toggle();
  $("#login-btn").addClass("active-login");
});

$( ".login-box-wrapper-toggle" ).mouseleave(function() {
  $( ".login-box" ).toggle();
  $( ".login-screen" ).toggle();
  $("#login-btn").removeClass("active-login");
});