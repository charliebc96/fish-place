$raiz = '';
if(window.location.href.indexOf("app_dev.php") > -1) {
    $raiz = '/app_dev.php';
}
$('#toggle-menu').click(function () {
    $('#menu').slideToggle();
});
$('#close-menu').click(function () {
    $('#menu').slideToggle();
});
$('.nslt-btn').click(function () {
    $('#form_newsletter').slideToggle();
});


$( function() {


    //$( "#fecha-res" ).datepicker();
    $('#fecha-res').datetimepicker({
        locale: 'es',
        //hoursDisabled: '0,1,2,3,4,5,6,7,8,9,10,11,22,23'
    });

    $('#new_reserva').submit(function(e){
        e.preventDefault();
        e.stopPropagation();
        var json = objectifyForm($(this).serializeArray());
        console.log(json);
            if(!validateForm()){
                alertar('Debes ingresar todos los campos');
            }else{
                $.LoadingOverlay("show",{ zIndex: 99999, image: '/js/jquery-loading-overlay/src/loading.gif'});
                var jqxhr = $.post( $raiz+"/api/reserva",json, function() {
                })
                    .done(function(data) {
                        $.LoadingOverlay("hide");
                        data = JSON.parse(data);
                        console.log(data);
                        if(data.ok){
                            alertar('Reserva guardada con éxito. El código de reserva es: '+data.codigo);
                            $('#new_reserva').trigger("reset");
                        }else{
                            var mens = data.error_msg;
                            alertar(mens);
                        }
                    })
                    .fail(function() {
                    })
                    .always(function() {
                    });
            }

    });

    $('#form_contacto').submit(function(e){
        e.preventDefault();
        e.stopPropagation();
        var json = objectifyForm($(this).serializeArray());
        console.log(json);
        if(!validateForm()){
            alertar('Debes ingresar todos los campos');
        }else{
            $.LoadingOverlay("show",{ zIndex: 99999, image: '/js/jquery-loading-overlay/src/loading.gif'});
            var jqxhr = $.post( $raiz+"/api/contacto",json, function() {
            })
                .done(function(data) {
                    $.LoadingOverlay("hide");
                    data = JSON.parse(data);
                    console.log(data);
                    if(data.ok){
                        alertar('Gracias por contactarnos');
                        $('#new_reserva,#form_contacto').trigger("reset");
                    }else{
                        var mens = data.error_msg;
                        alertar(mens);
                    }
                })
                .fail(function() {
                })
                .always(function() {
                });
        }

    });
} );

function alertar(msg){
    //alert(msg);
    alertify.alert('Mensaje',msg);
}

function objectifyForm(formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++){
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
function validateForm() {
    var isValid = true;
    $('#new_reserva input,#form_contacto input').each(function() {
        if($(this).attr("name") != 'rl'){
            if ( $(this).val() === '' ){
                console.log($(this).attr("name"),$(this).attr("id"));
                isValid = false;
            }
        }
    });
    if($('#acepto-pol:checked').length == 0)
        isValid = false;
    return isValid;
}
var swiperSedesHome = new Swiper('.sedes-home', {
    navigation: {
        nextEl: '.next-sedes-home',
        prevEl: '.prev-sedes-home',
    },
    keyboard: {
        enabled: true,
    },
});
var swiperResponsabilidad1 = new Swiper('.swip-res-1', {
    navigation: {
        nextEl: '.next-sedes-eventos',
        prevEl: '.prev-sedes-eventos',
    },
    keyboard: {
        enabled: true,
    },
});
var swiperResponsabilidad2 = new Swiper('.swip-res-2', {
    navigation: {
        nextEl: '.next-sedes-eventos2',
        prevEl: '.prev-sedes-eventos2',
    },
    keyboard: {
        enabled: true,
    },
});
var swiperEventos = new Swiper('.swiper-eventos', {
    navigation: {
        nextEl: '.next-sedes-eventos',
        prevEl: '.prev-sedes-eventos',
    },
    keyboard: {
        enabled: true,
    },
    hashNavigation: {
        watchState: true,
    },
});SostenibilidadEventos = new Swiper('.swiper-sostenibilidad', {
    keyboard: {
        enabled: true,
    },
    hashNavigation: {
        watchState: true,
    },
    simulateTouch: false
});
var swiperBlog = new Swiper('.swiper-blog', {
    slidesPerColumn: 3,
    spaceBetween: 30,
    simulateTouch: false,
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
            return '<span class="' + className + '">' + (index + 1) + '</span>';
        },
    },
    navigation: {
        nextEl: '.next-sedes-eventos',
        prevEl: '.prev-sedes-eventos',
    },
});
// var swiperTienda = new Swiper('.swip-tienda', {
//     slidesPerColumn: 3,
//     slidesPerView:3,
//     spaceBetween: 30,
//     simulateTouch: false,
//     pagination: {
//         el: '.swiper-pagination',l
//         clickable: true,
//         renderBullet: function (index, className) {
//             return '<span class="' + className + '">' + (index + 1) + '</span>';
//         },
//     },
//     navigation: {
//         nextEl: '.next-sedes-eventos',
//         prevEl: '.prev-sedes-eventos',
//     },
//     lazy:true
// });

if(isMobile()){
    $('.navegar').jPages({
        containerID:"swip-tienda",
        perPage: 10,
        first: false,
        previous: ".prev-sedes-eventos",
        next: ".next-sedes-eventos",
        last:false,

    });
}else{
    $('.navegar').jPages({
        containerID:"swip-tienda",
        perPage: 9,
        first: false,
        previous: ".prev-sedes-eventos",
        next: ".next-sedes-eventos",
        last:false,

    });
}
$('.nav-eventos-item').click(function () {
    $('.nav-eventos-item').each(function () {
        $(this).removeClass('active');
    });
    $(this).addClass('active');
});
var form_news;
function newsletter(){
    $('#form_newsletter').submit(function (e) {
        $.LoadingOverlay("show",{ zIndex: 9999, image: '/js/jquery-loading-overlay/src/loading.gif'});
        e.preventDefault();
        $('#terms_error,#news_email_error').hide();
        $acepto = $('#acepta').prop('checked');
        if($acepto){
            if(validateEmail($(this).find('input[type="email"]').val())){
                data = $(this).serialize();
                $.ajax({
                    url: $raiz+"/newsletter/"+$(this).find('input[type="email"]').val()
                }).done(function(data) {
                    $.LoadingOverlay("hide");
                    $('#exitoso').remove();
                    if(data.success == 1 || data.success == "1"){
                        $('#form_newsletter').prepend('<p id="exitoso">Inscrito exitosamente</p>');
                        $('#form_newsletter').find('input[type="email"]').val('');
                    }else{
                        $('#form_newsletter').prepend('<p id="exitoso" class="error">Email inscrito anteriormente</p>');
                    }
                });
            }else {
                $.LoadingOverlay("hide");
                $('#news_email_error').css('display','inline-block');
            }
        }else{
            $.LoadingOverlay("hide");
            $('#terms_error').css('display','inline-block');
        }
    });
}

function filtrarpost(elem,catid){
    $(".posts").hide();
    $(".post_"+catid).show();
    $(".cats").removeClass("catsactivo");
    $(elem).addClass("catsactivo");
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function seleccionarCantidadProducto(elem,ubId){
    var x = elem.value;
    $("#"+ubId).data("cant",x);

}

function cambiarmes(){
    var mes = $("#mes").val();
    var ano = $("#ano").val();
    if(mes == "" || ano == "" ){
        $(".posts").show();
        $(".cats").removeClass("catsactivo");
    }else{
        $(".cats").removeClass("catsactivo");
        $(".posts").hide();
        $(".post_fecha_"+mes+"_"+ano).show();
    }

}


$(function(){

    $(".centermap").click(function(){
        console.log($(this).data("lat"));
        console.log($(this).data("lng"));
        map.setCenter({lat: $(this).data("lat"), lng: $(this).data("lng")})
    });

    $(".share").jsSocials({
        shares: [ "twitter", "facebook"]
    });

    newsletter();
    if(isMobile()){
        // var slideout = new Slideout({
        //     'panel': document.getElementById('panel'),
        //     'menu': document.getElementById('menu'),
        //     'padding': 256,
        //     'tolerance': 70
        // });
        //
        // if($('.toggle-button').length > 0){
        //
        //     document.querySelector('.toggle-button').addEventListener('click', function() {
        //         slideout.toggle();
        //     });
        // }
    }

    $('#contacto').submit(function(e){
        e.preventDefault();
        console.log($('#nombre').val().length);
        console.log($('#apellido').val().length);
        console.log($('#telefono').val().length);
        console.log($('#email').val().length);
        console.log($('#acepta').val());
        if($('#nombre').val().length == 0 || $('#apellido').val().length == 0
            || $('#telefono').val().length == 0 || $('#email').val().length == 0 ){
            alertify.alert('Alert','You must enter all the fields');
        }else{
            if(!validateEmail($('#email').val()))
                alertify.alert('Alert','You must enter a valid email');
            else{
                if($('#acepta:checked').length == 0)
                    alertify.alert('Alerta','You must agree to terms and conditions');
                else{
                    database.ref('users_leaders/').push({
                        experiencia: $('#experiencia').val(),
                        nombre: $('#nombre').val(),
                        apellido: $('#apellido').val(),
                        telefono: $('#telefono').val(),
                        email: $('#email').val(),
                        created_at: new Date().toString()
                    });

                    alertify.alert('Alerta','Your data has been saved. Thank you');
                    $('#nombre').val(''),
                    $('#apellido').val(''),
                    $('#telefono').val(''),
                    $('#email').val('');
                }
            }
        }
    })

    $('#evento').submit(function(e){
        e.preventDefault();
        console.log($('#nombre').val().length);
        console.log($('#telefono').val().length);
        console.log($('#email').val().length);
        console.log($('#acepta').val());
        if($('#nombre').val().length == 0
            || $('#telefono').val().length == 0 || $('#email').val().length == 0 || $('#experiencia').val().length < 2){
            alertify.alert('Alert','You must enter all the obligatory fields');
        }else{
            if(!validateEmail($('#email').val()))
                alertify.alert('Alert','You must enter a valid email');
            else{
                if($('#acepta:checked').length == 0)
                    alertify.alert('Alerta','You must agree to terms and conditions');
                else{
                    database.ref('users_leaders/').push({
                        experiencia: $('#experiencia').val(),
                        industria: $('#industria').val(),
                        comentarios: $('#comentarios').val(),
                        nombre: $('#nombre').val(),
                        telefono: $('#telefono').val(),
                        email: $('#email').val(),
                        created_at: new Date().toString()
                    });

                    alertify.alert('Alerta','Your data has been saved. Thank you');
                    $('#nombre').val(''),
                    $('#apellido').val(''),
                    $('#telefono').val(''),
                    $('#email').val(''),
                    $('#industria').val(''),
                    $('#comentarios').val('');
                    $('#select').val(0);
                }
            }
        }
    })
$('#buscar-cat').click(function (e) {
    e.preventDefault();
    var ruta = $('#categoria-tienda').val();
    console.log(ruta);
    window.location.href = ruta;
});
    $('.regresar').click(function (e) {
        e.preventDefault();
        window.history.back();
    });

});


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


$( document ).ready(function() {

});


$( window ).resize(function() {

});


$(window).on("load", function() {

});

$(window).scroll(function(){

});



function isMobile() {
    return(/Android|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );
}

function isIpad(){
    return navigator.userAgent.match(/iPad/i);
}
