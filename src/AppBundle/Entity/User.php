<?php
namespace AppBundle\Entity;

use CarroiridianBundle\Entity\Compra;
use CarroiridianBundle\Entity\Envio;
use CarroiridianBundle\Entity\Producto;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use PagosPayuBundle\Entity\TokenPayu;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\TipoTarjeta")
     * @ORM\JoinColumn(name="tipotarjeta_id", referencedColumnName="id", nullable=true)
     */
    private $tipotarjeta;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="apellidos", type="string", length=255, nullable=true)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="cedula", type="string", length=255, nullable=true)
     */
    private $cedula;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Departamento")
     * @ORM\JoinColumn(name="departamento_id", referencedColumnName="id")
     */
    private $departamento;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Ciudad")
     * @ORM\JoinColumn(name="ciudad_id", referencedColumnName="id")
     * @Assert\NotBlank(message = "La ciudad no puede ser vacia")
     */
    private $ciudad;

    /**
     * @var string
     *
     * @ORM\Column(name="pais", type="string", length=255, nullable=true)
     */
    private $pais;

    /**
     * @var bool
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $tarjeta;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=true)
     */
    private $fecha;

    /**
     * @var Compra[]
     *
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Compra", mappedBy="comprador", cascade={"remove"})
     */
    private $compras;

    /**
     * @var Envio[]
     *
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Envio", mappedBy="user", cascade={"remove"})
     */
    private $direcciones;

    /**
     * @var Producto[]
     *
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Producto", mappedBy="user", cascade={"remove"})
     */
    private $favoritos;

    /**
     * @var TokenPayu[]
     *
     * @ORM\OneToMany(targetEntity="PagosPayuBundle\Entity\TokenPayu", mappedBy="usuario", cascade={"remove"})
     */
    private $tokens;

    public function __construct()
    {
        parent::__construct();
        $this->direcciones = new ArrayCollection();
        $this->compras = new ArrayCollection();
        $this->favoritos = new ArrayCollection();
    }

    public function __toString()
    {
        if(is_numeric($this->getId()))
            return $this->getId().'';
        return ' ';
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return User
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return User
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    
        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return User
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Add compra
     *
     * @param \CarroiridianBundle\Entity\Compra $compra
     *
     * @return User
     */
    public function addCompra(\CarroiridianBundle\Entity\Compra $compra)
    {
        $this->compras[] = $compra;
    
        return $this;
    }

    /**
     * Remove compra
     *
     * @param \CarroiridianBundle\Entity\Compra $compra
     */
    public function removeCompra(\CarroiridianBundle\Entity\Compra $compra)
    {
        $this->compras->removeElement($compra);
    }

    /**
     * Get compras
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompras()
    {
        return $this->compras;
    }

    /**
     * Add direccione
     *
     * @param \CarroiridianBundle\Entity\Envio $direccione
     *
     * @return User
     */
    public function addDireccione(\CarroiridianBundle\Entity\Envio $direccione)
    {
        $this->direcciones[] = $direccione;
    
        return $this;
    }

    /**
     * Remove direccione
     *
     * @param \CarroiridianBundle\Entity\Envio $direccione
     */
    public function removeDireccione(\CarroiridianBundle\Entity\Envio $direccione)
    {
        $this->direcciones->removeElement($direccione);
    }

    /**
     * Get direcciones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirecciones()
    {
        return $this->direcciones;
    }

    /**
     * Add favorito
     *
     * @param \CarroiridianBundle\Entity\Producto $favorito
     *
     * @return User
     */
    public function addFavorito(\CarroiridianBundle\Entity\Producto $favorito)
    {
        $this->favoritos[] = $favorito;
    
        return $this;
    }

    /**
     * Remove favorito
     *
     * @param \CarroiridianBundle\Entity\Producto $favorito
     */
    public function removeFavorito(\CarroiridianBundle\Entity\Producto $favorito)
    {
        $this->favoritos->removeElement($favorito);
    }

    /**
     * Get favoritos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavoritos()
    {
        return $this->favoritos;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return User
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Add token.
     *
     * @param \PagosPayuBundle\Entity\TokenPayu $token
     *
     * @return User
     */
    public function addToken(\PagosPayuBundle\Entity\TokenPayu $token)
    {
        $this->tokens[] = $token;
    
        return $this;
    }

    /**
     * Remove token.
     *
     * @param \PagosPayuBundle\Entity\TokenPayu $token
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeToken(\PagosPayuBundle\Entity\TokenPayu $token)
    {
        return $this->tokens->removeElement($token);
    }

    /**
     * Get tokens.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return User
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set ciudad
     *
     * @param string $ciudad
     *
     * @return User
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;
    
        return $this;
    }

    /**
     * Get ciudad
     *
     * @return string
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * Set pais
     *
     * @param string $pais
     *
     * @return User
     */
    public function setPais($pais)
    {
        $this->pais = $pais;
    
        return $this;
    }

    /**
     * Get pais
     *
     * @return string
     */
    public function getPais()
    {
        return $this->pais;
    }

    /**
     * Set tarjeta
     *
     * @param boolean $tarjeta
     *
     * @return User
     */
    public function setTarjeta($tarjeta)
    {
        $this->tarjeta = $tarjeta;
    
        return $this;
    }

    /**
     * Get tarjeta
     *
     * @return boolean
     */
    public function getTarjeta()
    {
        return $this->tarjeta;
    }

    /**
     * Set departamento
     *
     * @param \CarroiridianBundle\Entity\Departamento $departamento
     *
     * @return User
     */
    public function setDepartamento(\CarroiridianBundle\Entity\Departamento $departamento = null)
    {
        $this->departamento = $departamento;
    
        return $this;
    }

    /**
     * Get departamento
     *
     * @return \CarroiridianBundle\Entity\Departamento
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * Set tipotarjeta
     *
     * @param \CarroiridianBundle\Entity\TipoTarjeta $tipotarjeta
     *
     * @return User
     */
    public function setTipotarjeta(\CarroiridianBundle\Entity\TipoTarjeta $tipotarjeta = null)
    {
        $this->tipotarjeta = $tipotarjeta;
    
        return $this;
    }

    /**
     * Get tipotarjeta
     *
     * @return \CarroiridianBundle\Entity\TipoTarjeta
     */
    public function getTipotarjeta()
    {
        return $this->tipotarjeta;
    }

    /**
     * Set cedula
     *
     * @param string $cedula
     *
     * @return User
     */
    public function setCedula($cedula)
    {
        $this->cedula = $cedula;
    
        return $this;
    }

    /**
     * Get cedula
     *
     * @return string
     */
    public function getCedula()
    {
        return $this->cedula;
    }
}
