<?php
/**
 * Created by PhpStorm.
 * User: Iridian 4
 * Date: 26/07/2016
 * Time: 11:43 AM
 */

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\IsTrueValidator;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre',null,array('label'=>'Nombre'))
            ->add('apellido')
            ->add('email', EmailType::class)
            ->add('telefono')
            ->add('celular')
            ->add('pais')
            ->add('departamento')
            ->add('ciudad')
            ->add('area')
            ->add('mensaje')

            ->add('save', SubmitType::class, array('label' => 'Enviar','attr'=>array('class'=>'btn-enciar')));

        $builder->add('conditions', CheckboxType::class, array('mapped' => false,
            'constraints' => new IsTrue(array("message" => "Debes Aceptar términos y condiciones"))));
        $builder->add('datos', CheckboxType::class, array('mapped' => false,
            'constraints' => new IsTrue(array("message" => "Debes Aceptar politica de tratamiento"))));

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvents $event) {
            $data = $event->getData();
            if (!isset($data['conditions'])) {
                $data['conditions'] = false;
            }
            if (!isset($data['datos'])) {
                $data['datos'] = false;
            }
            $event->setData($data);
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ContactList',
            'locale' => 'en'
        ));
    }
}