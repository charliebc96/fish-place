<?php

namespace CompresoresBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/compresores", name="compresores")
     */
    public function indexAction()
    {
        return $this->render('CompresoresBundle:Default:index.html.twig');
    }
}
