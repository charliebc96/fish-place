<?php

namespace SostenibilidadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/sostenibilidad", name="sostenibilidad")
     */
    public function sostenibilidadAction()
    {
        return $this->render('SostenibilidadBundle:Default:index.html.twig');
    }
}
