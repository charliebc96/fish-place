<?php

namespace EventosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/eventos", name="eventos")
     */
    public function eventosAction()
    {
        return $this->render('EventosBundle:Default:index.html.twig');
    }
}
