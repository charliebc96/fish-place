<?php

namespace UserIridianBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nombre', TextType::class, array('required' => true));
        $builder->add('apellidos', TextType::class, array('required' => true));
        $builder->add('telefono', TextType::class, array('required' => true));
        $builder->add('cedula', TextType::class, array('required' => true));
        $builder->add('direccion', TextType::class, array('required' => true));
        $builder->add('departamento',EntityType::class,array(
            'class' => 'CarroiridianBundle\Entity\Departamento',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('d')
                    ->orderBy('d.nombre', 'ASC');
            }
        ))
        ->add('ciudad')
        ->add('username', HiddenType::class);
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';

        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    // For Symfony 2.x
    public function getNombre()
    {
        return $this->getBlockPrefix();
    }
}