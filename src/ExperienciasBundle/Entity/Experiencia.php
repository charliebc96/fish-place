<?php

namespace ExperienciasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Experiencia
 *
 * @ORM\Table(name="experiencia")
 * @ORM\Entity(repositoryClass="ExperienciasBundle\Repository\ExperienciaRepository")
 * @Vich\Uploadable
 */
class Experiencia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255)
     */
    private $logo;

    /**
     * @Vich\UploadableField(mapping="imagesgal", fileNameProperty="logo" )
     * @var File
     */
    private $logoFile;

    /**
     * @var string
     *
     * @ORM\Column(name="resumen_home_es", type="text", nullable=true)
     */
    private $resumenHomeEs;

    /**
     * @var string
     *
     * @ORM\Column(name="resumen_home_en", type="text", nullable=true)
     */
    private $resumenHomeEn;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_es", type="string", length=255)
     */
    private $nombreEs;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_en", type="string", length=255)
     */
    private $nombreEn;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden = 1;

    /**
     * @var bool
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible= true;

    /**
     * @var string
     *
     * @ORM\Column(name="video", type="string", length=255, nullable=true)
     */
    private $video;

    /**
     * @var string
     *
     * @ORM\Column(name="quote_es", type="string", length=255, nullable=true)
     */
    private $quoteEs;

    /**
     * @var string
     *
     * @ORM\Column(name="quote_en", type="string", length=255, nullable=true)
     */
    private $quoteEn;

    /**
     * @var string
     *
     * @ORM\Column(name="autor", type="string", length=255, nullable=true)
     */
    private $autor;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_es", type="text", nullable=true)
     */
    private $descripcionEs;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_en", type="text", nullable=true)
     */
    private $descripcionEn;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen_descripcion", type="string", length=255, nullable=true)
     */
    private $imagenDescripcion;

    /**
     * @Vich\UploadableField(mapping="imagesgal", fileNameProperty="imagenDescripcion" )
     * @var File
     */
    private $imagenDescripcionFile;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_inferior_es", type="text", nullable=true)
     */
    private $descripcionInferiorEs;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion_inferior_en", type="text", nullable=true)
     */
    private $descripcionInferiorEn;

    /**
     * One Experiencia has Many Mejoras.
     * @ORM\OneToMany(targetEntity="ExperienciasBundle\Entity\Mejora", mappedBy="experiencia", cascade={"persist"})
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    protected $mejoras;

    /**
     * @var string
     *
     * @ORM\Column(name="resumen_es", type="text", nullable=true)
     */
    private $resumenEs;

    /**
     * @var string
     *
     * @ORM\Column(name="resumen_en", type="text", nullable=true)
     */
    private $resumenEn;

    /**
     * @var string
     *
     * @ORM\Column(name="liderando_es", type="text", nullable=true)
     */
    private $liderandoEs;

    /**
     * @var string
     *
     * @ORM\Column(name="liderando_en", type="text", nullable=true)
     */
    private $liderandoEn;

    /**
     * @var string
     *
     * @ORM\Column(name="equipo_titulo_es", type="string", length=255, nullable=true)
     */
    private $equipoTituloEs;

    /**
     * @var string
     *
     * @ORM\Column(name="equipo_titulo_en", type="string", length=255, nullable=true)
     */
    private $equipoTituloEn;

    /**
     * @var string
     *
     * @ORM\Column(name="equipo_resumen_es", type="text", nullable=true)
     */
    private $equipoResumenEs;

    /**
     * @var string
     *
     * @ORM\Column(name="equipo_resumen_en", type="text", nullable=true)
     */
    private $equipoResumenEn;

    /**
     * @var string
     *
     * @ORM\Column(name="equipo_imagen", type="string", length=255, nullable=true)
     */
    private $equipoImagen;

    /**
     * @Vich\UploadableField(mapping="imagesgal", fileNameProperty="equipoImagen" )
     * @var File
     */
    private $equipoImagenFile;

    /**
     * @var string
     *
     * @ORM\Column(name="lat", type="string", length=255)
     */
    private $lat;

    /**
     * @var string
     *
     * @ORM\Column(name="lng", type="string", length=255)
     */
    private $lng;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="image_es", type="string", length=255)
     */
    private $imageEs;

    /**
     * @Vich\UploadableField(mapping="imagesgal", fileNameProperty="imageEs" )
     * @var File
     */
    private $imageEsFile;

    /**
     * @var string
     *
     * @ORM\Column(name="frase_contacto_es", type="string", length=255, nullable=true)
     */
    private $fraseContactoEs;

    /**
     * @var string
     *
     * @ORM\Column(name="frase_contacto_en", type="string", length=255, nullable=true)
     */
    private $fraseContactoEn;

    /**
     * One Experiencia has Many Mejoras.
     * @ORM\OneToMany(targetEntity="ExperienciasBundle\Entity\Mejora", mappedBy="experienciaequipo", cascade={"persist"})
     * @ORM\OrderBy({"orden" = "ASC"})
     */
    protected $equipos;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_resumen_es", type="string", length=255, nullable=true)
     */
    private $tituloResumenEs;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo_resumen_en", type="string", length=255, nullable=true)
     */
    private $tituloResumenEn;



    public function __construct()
    {
        $this->updatedAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    public function gen($campo,$locale){
        $accessor = PropertyAccess::createPropertyAccessor();
        return $accessor->getValue($this,$campo.'_'.$locale);
    }

    /**
     * Set nombreEs
     *
     * @param string $nombreEs
     *
     * @return Experiencia
     */
    public function setNombreEs($nombreEs)
    {
        $this->nombreEs = $nombreEs;
    
        return $this;
    }

    /**
     * Get nombreEs
     *
     * @return string
     */
    public function getNombreEs()
    {
        return $this->nombreEs;
    }

    /**
     * Set nombreEn
     *
     * @param string $nombreEn
     *
     * @return Experiencia
     */
    public function setNombreEn($nombreEn)
    {
        $this->nombreEn = $nombreEn;
    
        return $this;
    }

    /**
     * Get nombreEn
     *
     * @return string
     */
    public function getNombreEn()
    {
        return $this->nombreEn;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Experiencia
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    
        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set visble
     *
     * @param boolean $visble
     *
     * @return Experiencia
     */
    public function setVisble($visble)
    {
        $this->visble = $visble;
    
        return $this;
    }

    /**
     * Get visble
     *
     * @return boolean
     */
    public function getVisble()
    {
        return $this->visble;
    }

    /**
     * Set video
     *
     * @param string $video
     *
     * @return Experiencia
     */
    public function setVideo($video)
    {
        $this->video = $video;
    
        return $this;
    }

    /**
     * Get video
     *
     * @return string
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set quoteEs
     *
     * @param string $quoteEs
     *
     * @return Experiencia
     */
    public function setQuoteEs($quoteEs)
    {
        $this->quoteEs = $quoteEs;
    
        return $this;
    }

    /**
     * Get quoteEs
     *
     * @return string
     */
    public function getQuoteEs()
    {
        return $this->quoteEs;
    }

    /**
     * Set quoteEn
     *
     * @param string $quoteEn
     *
     * @return Experiencia
     */
    public function setQuoteEn($quoteEn)
    {
        $this->quoteEn = $quoteEn;
    
        return $this;
    }

    /**
     * Get quoteEn
     *
     * @return string
     */
    public function getQuoteEn()
    {
        return $this->quoteEn;
    }

    /**
     * Set autor
     *
     * @param string $autor
     *
     * @return Experiencia
     */
    public function setAutor($autor)
    {
        $this->autor = $autor;
    
        return $this;
    }

    /**
     * Get autor
     *
     * @return string
     */
    public function getAutor()
    {
        return $this->autor;
    }

    /**
     * Set descripcionEs
     *
     * @param string $descripcionEs
     *
     * @return Experiencia
     */
    public function setDescripcionEs($descripcionEs)
    {
        $this->descripcionEs = $descripcionEs;
    
        return $this;
    }

    /**
     * Get descripcionEs
     *
     * @return string
     */
    public function getDescripcionEs()
    {
        return $this->descripcionEs;
    }

    /**
     * Set descripcionEn
     *
     * @param string $descripcionEn
     *
     * @return Experiencia
     */
    public function setDescripcionEn($descripcionEn)
    {
        $this->descripcionEn = $descripcionEn;
    
        return $this;
    }

    /**
     * Get descripcionEn
     *
     * @return string
     */
    public function getDescripcionEn()
    {
        return $this->descripcionEn;
    }

    /**
     * Set imagenDescripcion
     *
     * @param string $imagenDescripcion
     *
     * @return Experiencia
     */
    public function setImagenDescripcion($imagenDescripcion)
    {
        $this->imagenDescripcion = $imagenDescripcion;
    
        return $this;
    }

    /**
     * Get imagenDescripcion
     *
     * @return string
     */
    public function getImagenDescripcion()
    {
        return $this->imagenDescripcion;
    }


    /**
     * Set imagenDescripcionFile
     *
     * @param string $imagenDescripcionFile
     *
     * @return Experiencia
     */
    public function setImagenDescripcionFile(File $image = null)
    {
        $this->imagenDescripcionFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * Get imagenDescripcionFile
     *
     * @return string
     */
    public function getImagenDescripcionFile()
    {
        return $this->imagenDescripcionFile;
    }

    /**
     * Set descripcionInferiorEs
     *
     * @param string $descripcionInferiorEs
     *
     * @return Experiencia
     */
    public function setDescripcionInferiorEs($descripcionInferiorEs)
    {
        $this->descripcionInferiorEs = $descripcionInferiorEs;
    
        return $this;
    }

    /**
     * Get descripcionInferiorEs
     *
     * @return string
     */
    public function getDescripcionInferiorEs()
    {
        return $this->descripcionInferiorEs;
    }

    /**
     * Set descripcionInferiorEn
     *
     * @param string $descripcionInferiorEn
     *
     * @return Experiencia
     */
    public function setDescripcionInferiorEn($descripcionInferiorEn)
    {
        $this->descripcionInferiorEn = $descripcionInferiorEn;
    
        return $this;
    }

    /**
     * Get descripcionInferiorEn
     *
     * @return string
     */
    public function getDescripcionInferiorEn()
    {
        return $this->descripcionInferiorEn;
    }

    /**
     * Set resumenEs
     *
     * @param string $resumenEs
     *
     * @return Experiencia
     */
    public function setResumenEs($resumenEs)
    {
        $this->resumenEs = $resumenEs;
    
        return $this;
    }

    /**
     * Get resumenEs
     *
     * @return string
     */
    public function getResumenEs()
    {
        return $this->resumenEs;
    }

    /**
     * Set resumenEn
     *
     * @param string $resumenEn
     *
     * @return Experiencia
     */
    public function setResumenEn($resumenEn)
    {
        $this->resumenEn = $resumenEn;
    
        return $this;
    }

    /**
     * Get resumenEn
     *
     * @return string
     */
    public function getResumenEn()
    {
        return $this->resumenEn;
    }

    /**
     * Set equipoTituloEs
     *
     * @param string $equipoTituloEs
     *
     * @return Experiencia
     */
    public function setEquipoTituloEs($equipoTituloEs)
    {
        $this->equipoTituloEs = $equipoTituloEs;
    
        return $this;
    }

    /**
     * Get equipoTituloEs
     *
     * @return string
     */
    public function getEquipoTituloEs()
    {
        return $this->equipoTituloEs;
    }

    /**
     * Set equipoTituloEn
     *
     * @param string $equipoTituloEn
     *
     * @return Experiencia
     */
    public function setEquipoTituloEn($equipoTituloEn)
    {
        $this->equipoTituloEn = $equipoTituloEn;
    
        return $this;
    }

    /**
     * Get equipoTituloEn
     *
     * @return string
     */
    public function getEquipoTituloEn()
    {
        return $this->equipoTituloEn;
    }

    /**
     * Set equipoResumenEs
     *
     * @param string $equipoResumenEs
     *
     * @return Experiencia
     */
    public function setEquipoResumenEs($equipoResumenEs)
    {
        $this->equipoResumenEs = $equipoResumenEs;
    
        return $this;
    }

    /**
     * Get equipoResumenEs
     *
     * @return string
     */
    public function getEquipoResumenEs()
    {
        return $this->equipoResumenEs;
    }

    /**
     * Set equipoResumenEn
     *
     * @param string $equipoResumenEn
     *
     * @return Experiencia
     */
    public function setEquipoResumenEn($equipoResumenEn)
    {
        $this->equipoResumenEn = $equipoResumenEn;
    
        return $this;
    }

    /**
     * Get equipoResumenEn
     *
     * @return string
     */
    public function getEquipoResumenEn()
    {
        return $this->equipoResumenEn;
    }

    /**
     * Set equipoImagen
     *
     * @param string $equipoImagen
     *
     * @return Experiencia
     */
    public function setEquipoImagen($equipoImagen)
    {
        $this->equipoImagen = $equipoImagen;
    
        return $this;
    }


    /**
     * Set equipoImagenFile
     *
     * @param string $equipoImagenFile
     *
     * @return Experiencia
     */
    public function setEquipoImagenFile(File $image = null)
    {
        $this->equipoImagenFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * Get equipoImagenFile
     *
     * @return string
     */
    public function getEquipoImagenFile()
    {
        return $this->equipoImagenFile;
    }

    /**
     * Get equipoImagen
     *
     * @return string
     */
    public function getEquipoImagen()
    {
        return $this->equipoImagen;
    }

    /**
     * Set lat
     *
     * @param string $lat
     *
     * @return Experiencia
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    
        return $this;
    }

    /**
     * Get lat
     *
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param string $lng
     *
     * @return Experiencia
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    
        return $this;
    }

    /**
     * Get lng
     *
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set logo
     *
     * @param string $logo
     *
     * @return Experiencia
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    
        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set logoFile
     *
     * @param string $logoFile
     *
     * @return Experiencia
     */
    public function setLogoFile(File $image = null)
    {
        $this->logoFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * Get logoFile
     *
     * @return string
     */
    public function getLogoFile()
    {
        return $this->logoFile;
    }

    /**
     * Set logobrandFile
     *
     * @param string $logobrandFile
     *
     * @return Experiencia
     */
    public function setLogobrandFile(File $image = null)
    {
        $this->logobranFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * Get logobrandFile
     *
     * @return string
     */
    public function getLogobrandFile()
    {
        return $this->logobrandFile;
    }

    /**
     * Set resumenHomeEs
     *
     * @param string $resumenHomeEs
     *
     * @return Experiencia
     */
    public function setResumenHomeEs($resumenHomeEs)
    {
        $this->resumenHomeEs = $resumenHomeEs;
    
        return $this;
    }

    /**
     * Get resumenHomeEs
     *
     * @return string
     */
    public function getResumenHomeEs()
    {
        return $this->resumenHomeEs;
    }

    /**
     * Set resumenHomeEn
     *
     * @param string $resumenHomeEn
     *
     * @return Experiencia
     */
    public function setResumenHomeEn($resumenHomeEn)
    {
        $this->resumenHomeEn = $resumenHomeEn;
    
        return $this;
    }

    /**
     * Get resumenHomeEn
     *
     * @return string
     */
    public function getResumenHomeEn()
    {
        return $this->resumenHomeEn;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Experiencia
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Experiencia
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add mejora
     *
     * @param \ExperienciasBundle\Entity\Mejora $mejora
     *
     * @return Experiencia
     */
    public function addMejora(\ExperienciasBundle\Entity\Mejora $mejora)
    {
        $mejora->setExperiencia($this);
        $this->mejoras[] = $mejora;
    
        return $this;
    }

    /**
     * Remove mejora
     *
     * @param \ExperienciasBundle\Entity\Mejora $mejora
     */
    public function removeMejora(\ExperienciasBundle\Entity\Mejora $mejora)
    {
        $this->mejoras->removeElement($mejora);
    }

    /**
     * Get mejoras
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMejoras()
    {
        return $this->mejoras;
    }

    /**
     * Set logobrand
     *
     * @param string $logobrand
     *
     * @return Experiencia
     */
    public function setLogobrand($logobrand)
    {
        $this->logobrand = $logobrand;
    
        return $this;
    }

    /**
     * Get logobrand
     *
     * @return string
     */
    public function getLogobrand()
    {
        return $this->logobrand;
    }

    /**
     * Set liderandoEs
     *
     * @param string $liderandoEs
     *
     * @return Experiencia
     */
    public function setLiderandoEs($liderandoEs)
    {
        $this->liderandoEs = $liderandoEs;
    
        return $this;
    }

    /**
     * Get liderandoEs
     *
     * @return string
     */
    public function getLiderandoEs()
    {
        return $this->liderandoEs;
    }

    /**
     * Set liderandoEn
     *
     * @param string $liderandoEn
     *
     * @return Experiencia
     */
    public function setLiderandoEn($liderandoEn)
    {
        $this->liderandoEn = $liderandoEn;
    
        return $this;
    }

    /**
     * Get liderandoEn
     *
     * @return string
     */
    public function getLiderandoEn()
    {
        return $this->liderandoEn;
    }


    /**
     * Set imageEsFile
     *
     * @param string $imageEsFile
     *
     * @return Experiencia
     */
    public function setImageEsFile(File $imageEs = null)
    {
        $this->imageEsFile = $imageEs;
        if ($imageEs) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * Get imageEsFile
     *
     * @return string
     */
    public function getImageEsFile()
    {
        return $this->imageEsFile;
    }

    /**
     * Set imageEs
     *
     * @param string $imageEs
     *
     * @return Experiencia
     */
    public function setImageEs($imageEs)
    {
        $this->imageEs = $imageEs;
    
        return $this;
    }

    /**
     * Get imageEs
     *
     * @return string
     */
    public function getImageEs()
    {
        return $this->imageEs;
    }

    /**
     * Set fraseContactoEs
     *
     * @param string $fraseContactoEs
     *
     * @return Experiencia
     */
    public function setFraseContactoEs($fraseContactoEs)
    {
        $this->fraseContactoEs = $fraseContactoEs;
    
        return $this;
    }

    /**
     * Get fraseContactoEs
     *
     * @return string
     */
    public function getFraseContactoEs()
    {
        return $this->fraseContactoEs;
    }

    /**
     * Set fraseContactoEn
     *
     * @param string $fraseContactoEn
     *
     * @return Experiencia
     */
    public function setFraseContactoEn($fraseContactoEn)
    {
        $this->fraseContactoEn = $fraseContactoEn;
    
        return $this;
    }

    /**
     * Get fraseContactoEn
     *
     * @return string
     */
    public function getFraseContactoEn()
    {
        return $this->fraseContactoEn;
    }

    /**
     * Add equipo
     *
     * @param \ExperienciasBundle\Entity\Mejora $equipo
     *
     * @return Experiencia
     */
    public function addEquipo(\ExperienciasBundle\Entity\Mejora $equipo)
    {
        $equipo->setExperienciaequipo($this);
        $this->equipos[] = $equipo;
    
        return $this;
    }

    /**
     * Remove equipo
     *
     * @param \ExperienciasBundle\Entity\Mejora $equipo
     */
    public function removeEquipo(\ExperienciasBundle\Entity\Mejora $equipo)
    {
        $this->equipos->removeElement($equipo);
    }

    /**
     * Get equipos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipos()
    {
        return $this->equipos;
    }

    /**
     * Set tituloResumenEs
     *
     * @param string $tituloResumenEs
     *
     * @return Experiencia
     */
    public function setTituloResumenEs($tituloResumenEs)
    {
        $this->tituloResumenEs = $tituloResumenEs;
    
        return $this;
    }

    /**
     * Get tituloResumenEs
     *
     * @return string
     */
    public function getTituloResumenEs()
    {
        return $this->tituloResumenEs;
    }

    /**
     * Set tituloResumenEn
     *
     * @param string $tituloResumenEn
     *
     * @return Experiencia
     */
    public function setTituloResumenEn($tituloResumenEn)
    {
        $this->tituloResumenEn = $tituloResumenEn;
    
        return $this;
    }

    /**
     * Get tituloResumenEn
     *
     * @return string
     */
    public function getTituloResumenEn()
    {
        return $this->tituloResumenEn;
    }
}
