<?php

namespace ExperienciasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Mejora
 *
 * @ORM\Table(name="mejora")
 * @ORM\Entity(repositoryClass="ExperienciasBundle\Repository\MejoraRepository")
 * @Vich\Uploadable
 */
class Mejora
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255)
     */
    private $imagen;

    /**
     * @Vich\UploadableField(mapping="imagesgal", fileNameProperty="imagen" )
     * @var File
     */
    private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_es", type="string", length=255)
     */
    private $nombreEs;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_en", type="string", length=255)
     */
    private $nombreEn;

    /**
     * @var string
     *
     * @ORM\Column(name="resumen_es", type="text", nullable=true)
     */
    private $resumenEs;

    /**
     * @var string
     *
     * @ORM\Column(name="resumen_en", type="text", nullable=true)
     */
    private $resumenEn;

    /**
     * @var int
     *
     * @ORM\Column(name="orden", type="integer")
     */
    private $orden;

    /**
     * @var bool
     *
     * @ORM\Column(name="visible", type="boolean")
     */
    private $visible;

    /**
     * @ORM\ManyToOne(targetEntity="ExperienciasBundle\Entity\Experiencia", inversedBy="mejoras")
     * @ORM\JoinColumn(name="experiencia_id", referencedColumnName="id")
     */
    protected $experiencia;

    /**
     * @ORM\ManyToOne(targetEntity="ExperienciasBundle\Entity\Experiencia", inversedBy="equipos")
     * @ORM\JoinColumn(name="experiencia_equipo_id", referencedColumnName="id")
     */
    protected $experienciaequipo;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;



    public function __construct()
    {
        $this->updatedAt = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    public function gen($campo,$locale){
        $accessor = PropertyAccess::createPropertyAccessor();
        return $accessor->getValue($this,$campo.'_'.$locale);
    }
    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return Mejora
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    
        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }


    /**
     * Set imageEsFile
     *
     * @param string $imageFile
     *
     * @return Mejora
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * Get imageFile
     *
     * @return string
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set nombreEs
     *
     * @param string $nombreEs
     *
     * @return Mejora
     */
    public function setNombreEs($nombreEs)
    {
        $this->nombreEs = $nombreEs;
    
        return $this;
    }

    /**
     * Get nombreEs
     *
     * @return string
     */
    public function getNombreEs()
    {
        return $this->nombreEs;
    }

    /**
     * Set nombreEn
     *
     * @param string $nombreEn
     *
     * @return Mejora
     */
    public function setNombreEn($nombreEn)
    {
        $this->nombreEn = $nombreEn;
    
        return $this;
    }

    /**
     * Get nombreEn
     *
     * @return string
     */
    public function getNombreEn()
    {
        return $this->nombreEn;
    }

    /**
     * Set resumenEs
     *
     * @param string $resumenEs
     *
     * @return Mejora
     */
    public function setResumenEs($resumenEs)
    {
        $this->resumenEs = $resumenEs;
    
        return $this;
    }

    /**
     * Get resumenEs
     *
     * @return string
     */
    public function getResumenEs()
    {
        return $this->resumenEs;
    }

    /**
     * Set resumenEn
     *
     * @param string $resumenEn
     *
     * @return Mejora
     */
    public function setResumenEn($resumenEn)
    {
        $this->resumenEn = $resumenEn;
    
        return $this;
    }

    /**
     * Get resumenEn
     *
     * @return string
     */
    public function getResumenEn()
    {
        return $this->resumenEn;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Mejora
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    
        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Mejora
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Mejora
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set experiencia
     *
     * @param \ExperienciasBundle\Entity\Experiencia $experiencia
     *
     * @return Mejora
     */
    public function setExperiencia(\ExperienciasBundle\Entity\Experiencia $experiencia = null)
    {
        $this->experiencia = $experiencia;
    
        return $this;
    }

    /**
     * Get experiencia
     *
     * @return \ExperienciasBundle\Entity\Experiencia
     */
    public function getExperiencia()
    {
        return $this->experiencia;
    }

    /**
     * Set experienciaequipo
     *
     * @param \ExperienciasBundle\Entity\Experiencia $experienciaequipo
     *
     * @return Mejora
     */
    public function setExperienciaequipo(\ExperienciasBundle\Entity\Experiencia $experienciaequipo = null)
    {
        $this->experienciaequipo = $experienciaequipo;
    
        return $this;
    }

    /**
     * Get experienciaequipo
     *
     * @return \ExperienciasBundle\Entity\Experiencia
     */
    public function getExperienciaequipo()
    {
        return $this->experienciaequipo;
    }
}
