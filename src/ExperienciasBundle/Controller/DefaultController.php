<?php

namespace ExperienciasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{


    /**
     * @Route("/experiencia/{id}/{nombre}", name="experiencias")
     */
    public function experienciaAction($id)
    {
        $experiencia = $this->getDoctrine()->getRepository('ExperienciasBundle:Experiencia')->find($id);
        $mejoras = $this->getDoctrine()->getRepository('ExperienciasBundle:Mejora')->findBy(array('experiencia'=>$experiencia,'visible'=>true),array('orden'=>'asc'));
        $equipos = $this->getDoctrine()->getRepository('ExperienciasBundle:Mejora')->findBy(array('experienciaequipo'=>$experiencia,'visible'=>true),array('orden'=>'asc'));
        return $this->render('ExperienciasBundle:Default:index.html.twig', array('experiencia'=>$experiencia, 'mejoras'=>$mejoras,'equipos'=>$equipos));
    }
}
