<?php
/**
 * Created by PhpStorm.
 * User: Iridian 4
 * Date: 26/07/2016
 * Time: 11:43 AM
 */

namespace BlogiridianBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
class ComentarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mensaje',null,array('label'=>false,'attr'=>array('placeholder'=>'Tu comentario')))
            ->add('nombre',null,array('label'=>false,'attr'=>array('placeholder'=>'Tu nombre')))
            ->add('email',EmailType::class,array('label'=>false,'attr'=>array('placeholder'=>'Tu correo')))
            ->add('save', SubmitType::class, array('label' => 'Enviar','attr'=>array('class'=>'')));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BlogiridianBundle\Entity\Comentario',
            'locale' => 'en'
        ));
    }
}