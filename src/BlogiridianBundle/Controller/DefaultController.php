<?php

namespace BlogiridianBundle\Controller;

use BlogiridianBundle\Entity\Comentario;
use BlogiridianBundle\Form\Type\ComentarioType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/recetas-producto/{productoId}", name="recetas_prod")
     */
    public function recetasprodAction(Request $request,$productoId)
    {
        $qi = $this->get('qi');
        $term = $request->get('q');
        $year = $request->get('year');
        $month = $request->get('month');
        $repo_post = $this->getDoctrine()->getRepository('BlogiridianBundle:Post');
        $prod= $this->getDoctrine()->getRepository("CarroiridianBundle:Producto")->find($productoId);
        if($term)
            $ultimos = $qi->getResultadosBlog($term,null,$year,$month,$prod,null);
        else
            $ultimos = $qi->getResultadosBlog(null,10,$year,$month,$prod,null);
        $todos = $repo_post->findBy(array('visible'=>true),array('fecha'=>'desc'));
        $categorias =  $this->getDoctrine()->getRepository("BlogiridianBundle:CategoriaBlog")->findBy(array("visible"=>"1"),array("orden"=>"asc"));
        return $this->render('BlogiridianBundle:Default:index.html.twig',array('ultimos'=>$ultimos,'todos'=>$todos,"categorias"=>$categorias));
    }
    /**
     * @Route("/blog", name="the_blog")
     * @Route("/categoria_blog/{categoria_id/{nombre}", name="categoria_blog")
     */
    public function indexAction(Request $request)
    {
        $qi = $this->get('qi');
        $term = $request->get('q');
        $year = $request->get('year');
        $month = $request->get('month');
        $search = $request->get('search');

        $repo_post = $this->getDoctrine()->getRepository('BlogiridianBundle:Post');
        if($term)
            $ultimos = $qi->getResultadosBlog($term,null,$year,$month,null,null,$search);
        else
            $ultimos = $qi->getResultadosBlog(null,null,$year,$month,null,null,$search);
        $todos = $repo_post->findBy(array('visible'=>true),array('fecha'=>'desc'));
        $categorias =  $this->getDoctrine()->getRepository("BlogiridianBundle:CategoriaBlog")->findBy(array("visible"=>"1"),array("orden"=>"asc"));
        return $this->render('BlogiridianBundle:Default:index.html.twig',array('ultimos'=>$ultimos,'todos'=>$todos,"categorias"=>$categorias,"search"=>$search));
    }

    /**
     * @Route("/post/{id}/{name}", name="post")
     */
    public function postAction(Request $request,$id)
    {
        date_default_timezone_set("America/Bogota");
        $repo_post = $this->getDoctrine()->getRepository('BlogiridianBundle:Post');
        $repo_gal = $this->getDoctrine()->getRepository('BlogiridianBundle:GaleriaPost');
        $post = $repo_post->findOneBy(array('visible'=>true,'id'=>$id),array('fecha'=>'desc'),2);
        $gals = $repo_gal->findBy(array('post'=>$id,'visible'=>true),array('orden'=>'asc'));
        $imagenes = array();
        array_push($imagenes,$post->getImage());
        foreach ($gals as $gal){
            array_push($imagenes,$gal->getImagen());
        }

        $gracias = false;
        $comentario = new Comentario();
        $comentario->setPost($post);
        $form = $this->createForm(ComentarioType::class,$comentario);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $gracias = true;
            $qi = $this->get('qi');
            $em = $this->getDoctrine()->getManager();
            $em->persist($comentario);
            $em->flush();
            $comentario = new Comentario();
            $form = $this->createForm(ComentarioType::class,$comentario);
        }
        $comentarios = $this->getDoctrine()->getRepository('BlogiridianBundle:Comentario')->findBy(array('post'=>$post,'visible'=>true), array('createdAt'=>'desc'));
        $todos = $repo_post->findBy(array('visible'=>true),array('fecha'=>'desc'),15);
        return $this->render('BlogiridianBundle:Default:the_blog.html.twig',array('post'=>$post,'imagenes'=>$imagenes,'form'=>$form->createView(),'gracias'=>$gracias, 'comentarios'=>$comentarios,'todos'=>$todos,'gracias'=>$gracias));
    }
}
