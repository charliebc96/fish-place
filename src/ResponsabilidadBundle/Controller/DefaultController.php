<?php

namespace ResponsabilidadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/responsabilidad-social", name="responsabilidad")
     */
    public function responsabilidadAction()
    {
        return $this->render('ResponsabilidadBundle:Default:index.html.twig');
    }
}
