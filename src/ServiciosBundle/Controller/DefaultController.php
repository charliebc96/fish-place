<?php

namespace ServiciosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/servicios", name="Servicios")
     */
    public function indexAction()
    {
        return $this->render('ServiciosBundle:Default:index.html.twig');
    }
}
